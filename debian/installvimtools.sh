#!/bin/bash

# Vimのインストール

InstallVim()
{
    # Vim本体のインストール
    sudo sh -c "apt-get install vim"

    # 自分のvimスクリプトのインストール
    git clone -b dein git@bitbucket.org:Anomino/settingfiles.git
}


InitVim()
{
    # Vimのドットファイルのシンボリックリンクの作成
    ln -s ~/settingfiles/.vim ~/.vim
    ln -s ~/settingfiles/.vimrc ~/.vimrc

    rm -rf ~/settingfiles/.vim/dein/repos/github.com/*
}


InstallVim
InitVim
