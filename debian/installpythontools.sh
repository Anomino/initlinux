#!/bin/bash
#########################################
# Python3用の開発環境などのインストール #
#########################################

# 1. Pyenvのインストール
# 2. その他、必要そうなライブラリのインストール


# 引数にpyenvの初期設定スクリプトファイルを取る
InstallPyenv()
{
    # pyenvを落としてくる。
    git clone https://github.com/yyuu/pyenv ~/.pyenv

    # 依存ライブラリのインストール
    sudo sh -c "apt-get install -y make build-essential libssl-dev zlib1g-dev libbz2-dev"
    sudo sh -c "apt-get install -y libreadline-dev libsqlite3-dev wget curl llvm"

    # 推奨されてたので、NumPy、SciPy、matplotlibのインストール
    sudo sh -c "apt-get install -y libfreetype6-dev libblas-dev liblapack-dev gfortran tk-dev"
    
    cat pyenv_profile.txt > $1


}


# PyenvにPython本体のインストール
InstallPython()
{
    pyenv install 3.5.2
}


InstallLibrary()
{
    # pipによって必要なライブラリのインストール
    pip install --upgrade pip
    pip install numpy
    pip install scipy
    pip install matplotlib
    pip install scikit-learn
    pip install pandas
    pip install requests_oauthlib
}


InstallPyenv .bash_profile
InstallPython
InstallLibrary
