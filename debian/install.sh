#!/bin/bash

Main()
{
    # カレントディレクトリの全ファイルにパーミッションを与える。
    echo "必要なスクリプトのインストールの為、Root権限を必要とします。"
    #chmod 700 ./*
    #sudo chown root ./*

    InitLinux $1 $2
    InputJapanese
    InstallPython
    InstallVim
    InstallOther

    #sudo chown $3 *
    echo "すべての処理を終了しました。"
}


InstallPython()
{
    clear
    echo "Pythonと周辺ライブラリをインストールします。"
    ./installpythontools.sh
    echo "Pythonの開発環境が整いました。"
}


InstallVim()
{
    clear
    echo "Vimと、Vimスクリプトをインストールします。"
    ./installvimtools.sh
    echo "Vimの開発環境が整いました。"
}


# 汎用ソフトの一括インストール
InstallOther()
{
    clear
    echo "汎用ソフトのインストールをします。"

    # トップダウン型のターミナルソフト
    #sudo sh -c "apt-get install guake"
    sudo sh -c "apt-get install yakuake"
    
    # 愛用している音楽再生ソフト
    sudo sh -c "apt-get install banshee"

    # cd書き込みソフト
    sudo sh -c "apt-get install brasero"

    # OSのダウンロードに
    sudo sh -c "apt-get install deluge"

    # debパッケージのインストーラ
    sudo sh -c "apt-get install gdebi"
}


# 日本語入力設定
InputJapanese()
{
    clear
    echo "日本語入力の設定をします。"

    sudo sh -c "apt install fcitx-mozc --install-recommends"
    im-config -n fcitx
}


InitLinux()
{
    # パッケージを最新に
    sudo sh -c "apt-get dist-upgrade"

    # これからのインストールに必要なものを先にインストールしておく
    ./installgittools.sh

    # 日本語表示
    case $1 in
        "mint")
            # LinuxMint
            wget -q http://packages.linuxmint-jp.net/linuxmint-ja-archive-keyring.gpg -O- | sudo sh -c "apt-key add -"
            sudo sh -c "wget http://packages.linuxmint-jp.net/sources.list.d/linuxmint-ja.list -O /etc/apt/sources.list.d/linuxmint-ja.list"
            sudo sh -c "apt-get update"
            sudo sh -c "apt-get dist-upgrade"

            case $2 in
                "kde")
                    #KDE
                    sudo sh -c "apt-get install mint-kde-ja --install-recommends"
                    ;;

                "mate")
                    # MATE
                    sudo sh -c "apt-get install mint-gnome-ja --install-recommends"
                    ;;

                "cinnamon")
                    # Cinnamon
                    sudo sh -c "apt-get install mint-gnome-ja --install-recommends"
                    ;;

                "xfce")
                    # Xfce
                    sudo sh -c "apt-get install mint-gnome-ja --install-recommends"
                    ;;

                "lmde")
                    # LMDE
                    sudo sh -c "apt-get install mint-lmde-ja --install-recommends"
                    im-config -n fcitx
                    ;;

            esac

            ;;
        "ubuntu")
            # Ubuntu
            ;;

    esac

    # デフォルトディレクトリの設定
    mkdir ~/Documents
    mkdir ~/Downloads
    mkdir ~/Music
    mkdir ~/Pictures
    mkdir ~/Videos

    # うざったい日本語表記のディレクトリにシンボリックリンク
    ln -s ~/Documents ~/ドキュメント
    ln -s ~/Downloads ~/ダウンロード
    ln -s ~/Music ~/ミュージック
    ln -s ~/Pictures ~/ピクチャ
    ln -s ~/Videos ~/ビデオ
}


if test $# -lt 2
then
    echo "Usage : [command] [linux-dist-name] [gui-name]"
    exit
fi

Main $1 $2
